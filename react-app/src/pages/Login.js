import {useState} from 'react';
import { Form, Button } from 'react-bootstrap';

export function Login() {

  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [isActive, setIsActive] = useState(false);

  console.log(email);
  console.log(password1);

  function loginUser(e) {
    e.preventDefault()

    setEmail("");
    setPassword1("");

    alert('You are now logged in!');
  }

  useEffect(() => {
    if (email !== '' && password1 !== ''){
      setIsActive(true);
    } else {
      setIsActive(false);

    }
  }, [email, password1])

function loginDetails() {
 return (
	 <Form>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
          type="email" 
          placeholder="Enter email"
          value={email}
          onChange={e => setEmail(e.target.value)}
          required
           />
        <Form.Text className="text-muted">
          Please input email address.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>

        <Form.Control 
        type="password" 
        placeholder="Password"
        value={setPassword1} 
        onChange={e => setPassword1(e.target.value)}
        required
        />
      </Form.Group>

export default Login;